# Hydroponique
![Home](./home.PNG)
## Projet de culture hydroponique  
[Lien vers le serveur de démonstration](http://plantum.tk)  
[Résumés des mêlées quotidiennes](https://gitlab.com/hydroponique/hydroponique/-/blob/dev/scrumMeeting.md)

## Table des matières
- [Installation](#installation)
- [Dictionnaire](#dictionnaire)
- [Rôle](#rôle)
- [Branches et GitFlow](#branches-et-gitflow)
- [Tests](#tests)
- [TP1](#tp1)
- [Hardware](#tutoriel-de-mise-en-place-du-Hardware)
- [Liens utiles](#lien-utiles)

# Installation

1. Faire la configuration de **[WSL2](https://docs.docker.com/docker-for-windows/wsl/)** pour Docker avec Ubuntu-20.04.

2. Cloner le dossier gitlab en **[https](https://gitlab.com/hydroponique/hydroponique.git)** ou en ssh dans vos fichiers Linux:
- `git clone https://gitlab.com/hydroponique/hydroponique.git`
- `cd hydroponique/`
- `sudo git checkout nomBranche`

3. Si c'est la première exécution, roulez tous les conteneurs à l'aide du script `fresh_start.sh`, sinon faite la commande `docker-compose up -d`

4. Aller à l'adresse http://localhost/ pour accéder à la page 'Login' de l'application.

# Connection pgAdmin
1. Aller à l'adresse http://localhost:3001/ pour accéder au login.

2. Entrer les informations de connection se trouvant dans le **[fichier docker-compose.yml](https://gitlab.com/hydroponique/hydroponique/-/blob/dev/docker-compose.yml)** sous pgadmin/environnement.

3. Dans la section quicks links , appuyer sur 'add new server' , puis entrez les information demandées dans l'onglet connection à l'aide du **[fichier de configuration](https://gitlab.com/hydroponique/hydroponique/-/blob/dev/apihydroponique/database/config.py)** de la database.

![Connexion PgAdmin](./serverConnexionPgAdmin.JPG)
# Dictionnaire

- <b>Alerte</b> : Alerte urgente en rapport à une système (luminosité, température, eau, etc).
- <b>Notification</b> : Notification en rapport aux utilisateurs de l'application.
- <b>Système</b> : Un système hydroponique contient un ou plusieurs plants. Le système est réalisée sur un substrat neutre et inerte qui est régulièrement irrigué d’un courant de solution qui apporte les sels minéraux et des nutriments essentiels aux plantes.  
- <b>Culture</b> : Une culture hydroponique contient un ou plusieurs système.
- <b>Photopériode</b> : La photopériode est le rapport entre la durée du jour et la durée de la nuit.
- <b>Horticulteur</b> : Personne qui pratique l’art de cultiver.
- <b>Bac/Bassin de culture</b> : contenant adapté à préparer les semis et à cultiver les différentes plantations.  
- <b>Ferme urbaine</b> : Agriculture se situant en ville (à l’extérieur ou à l’intérieur des bâtiments).
- <b>Agriculture verticale</b> : agriculture sur des structures verticales afin de maximiser l’espace au sol et faciliter le travail de l’agriculteur (ergonomie).
- <b>Denrée</b> : Produit comestible servant à l’alimentation de l’homme.
- <b>Senseur</b> : Dispositif optoélectronique permettant de visualiser une valeur réelle.

# Rôle

- <b>François-Nicolas</b> : Scrum master, Product owner
- <b>Nicolas</b> : Dev-FE, Lead
- <b>Paul</b> : Contact-Client, Ops
- <b>Samuel C.</b> : Product owner
- <b>Samuel G.</b> : Dev-BE, QA

# Branches et GitFlow

- <b>dev</b> : Branche commune de développement des nouvelles fonctionalités.
- <b>dev-paul/dev-sam/dev-nic</b> : Branche personelle de chacun des développeurs dans laquelle il développe les nouvelles fonctionnalités avant de les intégrer à la banche commune <b>dev</b>.
- <b>test</b> : Branche contenant le code de test.
- <b>staging</b> : Branche comprenant le code servant à la démo client.
- <b>master</b> : Branche hébergant le code servant à la mise en production.

![Gitflow](./Gitflow.jpg)

# Tests

Pour lancer les tests, lancer le script tests.sh.  

# TP1
## Diagramme du développement continu DevOps
### Schéma
![PipelineDiagram](./PipelineDiagram.jpeg)
## Explication
#### GitLab:
- dev
- test
- staging
- master
#### AzureDevops:
- Hydroponique-CI : Surveille la branche test du Gitlab à un interval régulier de 180s afin de détecter tout changement et effectue les actions suivante lorsque un changement est présent.
    - build : lancer a l'aide d'une commande <b>docker-compose up -d</b>
    - Running test : lance le script <b>pipeline_tests.sh</b>
    - Merge to staging
- Hydroponique-CD : Surveille la branche staging du Gitlab à un interval régulier de 180s afin de détecter tout changement et effectue les actions suivante lorsque un changement est présent.
    - Pull staging
    - Connect to AWS with ssh
    - Run startCD.sh on AWS
#### AWS: serveur de demo
- Build les conteneurs sur le serveur. 

## Scripts

- fresh_start.sh
```
docker container prune -f
docker rmi hydroponique_db
docker rmi hydroponique_fronttest_1
docker rmi hydroponique_apihydroponique
docker rmi hydroponique_fronthydroponique
docker rmi dpage/pgadmin4
docker volume prune -f
docker-compose up --build -d
```

Supprime tous les conteneurs arrêtés, supprime les images utilisé par le projet, supprime tous les volumes non-utilisés, lance le projet en mode détaché


- tests.sh:
```
docker exec -it hydroponique_apihydroponique_1 python -m pytest

cd fronthydroponique
docker build . -f test.Dockerfile -t hydroponique_fronttest_1
cd ..
docker run --name react-app-test --rm -p 5002:5002 -e CI=true -v $PWD/fronthydroponique:/app -v /app/node_modules --net default hydroponique_fronttest_1
```

Lance les tests de l'API dans le conteneur de ce dernier, build l'image pour les tests front-end, lance le conteneur à l'aide de l'image créé juste avant.

- pipeline_tests.sh:  
```
# API_TEST
if docker exec -it s_apihydroponique_1 python -m pytest | grep "failed"
then 
    exit 1
fi

# FRONT_TEST
cd fronthydroponique
docker build . -f test.Dockerfile -t s_fronttest
cd ..
docker run --name react-app-test --rm -p 5002:5002 -e CI=true -v $PWD/fronthydroponique:/app -v /app/node_modules --net s_default s_fronttest
```

Exécute les tests du backend en vérifiant si il y a une erreur "failed" générée. Si une erreur est détéctée le pipeline s'arrète avec le signalement de l'erreur.  
Puis exécute les tests du front-end. Si il y a une erreur lancée par npm, le pipeline s'arrète avec le signalement de l'erreur.  

- startCD.sh:  
```
cd hydroponique
docker-compose down
cd ..
rm -r hydroponique -f

git clone 'https://naxis%40live.ca:D8c77d48d@gitlab.com/hydroponique/hydroponique/'
cd hydroponique
git checkout staging


docker-compose up --build -d 2> ../docker-compose.log
```

Ferme les containers dans le dossier hydroponique du server.  
Mis à jour du code de l'application à partir de GitLab en supprimant les fichiers et en clonant le projet puis se déplace sur la branche staging. 
Démarrage des containers en ignorant les erreurs généré par le terminal SH_UNIX.  

- cleanup.sh:  
```
cd /home/ubuntu/hydroponique/
docker-compose down

docker system prune -f
docker volume prune -f

docker-compose up -d
```

Script qui est lancé tous les jours à 1h du matin afin de libérer de l'espace sur le serveur AWS.  
Désactive le serveur, system prune, volume prune, et relance le serveur
# Tutoriel de mise en place du Hardware
Pour tout ce qui se rattache au élément physique requis, tel que le Raspberry Pi , le Arduino , les senseurs,  
ainsi que les nombreux autres compossants électrique, il est importantde se réferer directement sur le site du fabricant  
si quoi que ce soit divers de ce que présenter ci-dessous.  
  
  
### Étape 1: Préparation de l'arduino
Dépendament de vos senseurs, differentes librairis de code peuvent être nécessaire afin de faire fonctionné ces derniers. Voici un guide expliquant  
comment [ajouté de nouvelles librairis à votre arduino:](https://www.arduino.cc/en/guide/libraries)
### Étape 2: Branchement des différents senseurs
Pour ce qui est des branchements à effectuer, il divers également dépendament de vos achats. Voici donc quelques images de nos connections actuelle afin de vous aider  
dans vos démarches:
[Image1](./connection_1.jpg)
[Image2](./connection_2.jpg)
[Image3](./connection_3.jpg)
[Image4](./connection_4.jpg)
[Image5](./connection_5.jpg)
### Étape 3: Téléverser le code pour Arduino
Félicitations !  
Vous êtes maintenant rendu à la dernière et plus facile étape: mettre à jour le code de votre arduino afin de recolter les données récupéré par les senseurs.
Pour ce faire, il suffit de copier/coller le code fourni dans votre IDE de programmation arduino, puis de téleverser celui-ci.  
#### [Arduino Code Snippet](https://gitlab.com/hydroponique/hydroponique/-/snippets/2083947)
### Étape 4: Préparation du Raspberry Pi
Cette étape est probablement la plus longue. Nous prendrons donc la voie facile et te redigigeons vers ce merveilleux guide déja tout prêt  
#### [Guide](https://www.instructables.com/Raspberry-Pi-Arduino-Serial-Communication/)  
Et puis ? on ta eu ?
### Étape 5: Fonctionnement du script RaspberryPiScript.py


## Mise en place du service d'hébergement AWS  
Pour la mise en place AWS, on a utilisé la procédure suivante:  
[Installation AWS](https://gitlab.com/hydroponique/hydroponique/-/blob/dev/Installation%20AWS%20Linux%20Ubuntu.pdf)  

### Ports
80 : FrontEnd  
3001 : PgAdmin  
5000 : BackEnd  
Ses ports sont accessibles seulement avec nos IP personnelles afin de ne pas subir d'attaque indésirable qui nuiraient au bon fonctionnement du serveur Staging.
### Diagramme Modele Database
![DiagrammeModelDatabase](./dbm.PNG)
## Lien utiles 
[Azure DevOps](https://dev.azure.com/1666046/hydroponie)  
[Trello](https://trello.com/b/y4ZoXAbp/hydroponique)  
[GitLab Repository](https://gitlab.com/hydroponique/hydroponique)  
[AWS](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Home:)  
[Package d'Internationalisation du Site Web](https://react.i18next.com)

## Outils,Logiciels utilisés
